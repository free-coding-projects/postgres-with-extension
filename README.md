# Postgres With Extension

Custom Docker image which can enable extensions via comma seperated env var

#### Build and run the Docker image

* Build the image
```
docker build -t postgresextdb .
```

* Run with docker-compose file, which starts postgres, amq server and adminer (DB webclient)
```
docker-compose -up d
``` 
* Run with already started postgres db
```
docker run --rm -d -p 5432:5432 postgresextdb 
```
#### Available environment variables
Set DB_EXTENSIONS for example uuid-ossp,uuid-ossp
* DB_EXTENSIONS uuid-ossp
