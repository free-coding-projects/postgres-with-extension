#!/bin/bash
set -e

result=""
for ext in ${DB_EXTENSIONS//,/ }
do
    # call your procedure/other scripts here below
    result+="CREATE EXTENSION IF NOT EXISTS \"$ext\";";
done

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname="$POSTGRES_DB"<<-EOSQL
   ${result}
EOSQL
